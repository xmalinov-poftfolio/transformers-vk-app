var gulp = require('gulp');
var browserSync = require('browser-sync');
var del = require('del');
var concat = require('gulp-concat');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var minifyHtml = require('gulp-minify-html');
var gulpIf = require('gulp-if');

var pathsDev = {
  scripts: ['./src/js/**/*.js'],
  styles: ['./src/css/**/*.css'],
  html: ['./src/*.html'],
  images: ['./src/img/**/*.*'],
  extras: ['./src/crossdomain.xml', './src/humans.txt', './src/robots.txt', './src/favicon.ico']
};

var pathsBuild = {
  scripts: ['./build/js/**/*.js'],
  styles: ['./build/css/**/*.css'],
  root: ['./build/*.html'],
  images: ['./build/img/**/*.*']
};

var pathsDist = {
  scripts: ['./dist/js/'],
  styles: ['./dist/css/'],
  root: ['./dist/'],
  images: ['./dist/img/']
};

// static server
gulp.task('serve', function () {
  browserSync({
    logLevel: 'silent',
    port: 9000,
    server: {
      baseDir: './build/'
    }
  });
});

// changes of static files
gulp.task('watch', function () {
  gulp.watch([
    pathsBuild.root,
    pathsBuild.scripts,
    pathsBuild.styles,
    pathsBuild.images
  ]).on('change', browserSync.reload);
});

// changes of static css files for prefixes
//TODO change autoprefixing in build
gulp.task('watchcss', function () {
  gulp.watch([pathsDev.styles], ['autoprefix']);
});

// autoprefixer
gulp.task('autoprefix', function () {
  var autoprefixer = require('gulp-autoprefixer');
  var browsersArr = ['last 2 version', '> 1%', 'ie > 8'];

  //check browsers
  //var info = require('autoprefixer')({browsers: ['last 2 version', '> 1%', 'ie > 8']}).info();
  //console.log(info);

  return gulp.src(pathsDev.styles)
    .pipe(autoprefixer({
      browsers: browsersArr,
      cascade: false
    }))
    .pipe(gulp.dest('./src/css/'));
});

// compress images using tinypng.com
//TODO create api-key changing
gulp.task('build:tinypng', function () {
  var tinypng = require('gulp-tinypng');
  return gulp.src(pathsDev.images)
    .pipe(tinypng('wglUzGPbSzt-a-DMRCwd5mztAaKJJwB9'))
    .pipe(gulp.dest(pathsBuild.images[0]));
});

// pack js and css in html files, minify all html/js/css
gulp.task('minifyall', function () {
  return gulp.src(pathsDev.html)
    .pipe(useref())
    .pipe(gulpIf('*.html', minifyHtml()))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', minifyCss()))
    .pipe(gulp.dest('build'));
});

// clean dist directory
gulp.task('clean:dist', function () {
  del('./dist');
});

// clean build directory
gulp.task('clean:build', function () {
  del('./build');
});

gulp.task('clean-one', function () {
  del('dist/css');
  del('dist/js');
  del('dist/scripts');

  var minifyHtml = require('gulp-minify-html');

  return gulp.src('dist/*.html')
    .pipe(minifyHtml())
    .pipe(gulp.dest('dist'));
});

gulp.task('useref', function () {
  var options = {
    compress: false
  };

  return gulp.src(pathsDev.html)
    .pipe(useref()).pipe(gulp.dest('dist'));
});

gulp.task('onefile', ['useref'], function () {
  var inlinesource = require('gulp-inline-source');

  var options = {
    compress: true
  };

  return gulp.src('./dist/*.html')
    .pipe(inlinesource(options))
    .pipe(gulp.dest('./dist'));
});

// compress images
gulp.task('imagemin', function () {
  var imagemin = require('gulp-imagemin');
  var pngquant = require('imagemin-pngquant');

  return gulp.src('./src/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      interlaced: true,
      use: [pngquant()]
    }))
    .pipe(gulp.dest('dist'));
});

// less watch and compile
gulp.task('less', function () {
  var less = require('gulp-less');
  gulp.src('./src/css/less/**/*.less')
    .pipe(less())
    .pipe(concat('main-less.css'))
    .pipe(gulp.dest('./src/css'));
});

// changes of in less files
gulp.task('watchless', function () {
  gulp.watch(['./src/css/less/**/*.less'], ['less']);
});

gulp.task('build:html', function () {
  return gulp.src(['./src/*.html', '!./src/index.html'])
    .pipe(gulp.dest('./build'));
});

gulp.task('build:css', function () {
  return gulp.src('./src/css/**/*.css')
    .pipe(concat('main.css'))
    .pipe(gulp.dest('./build/css'));
});

gulp.task('build:js', function () {
  return gulp.src(['./src/js/event.js', './src/js/utils.js', './src/js/main.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./build/js'));
});

gulp.task('build:js-vendors', function () {
  return gulp.src(['./bower_components/angular/angular.min.js'])
    .pipe(gulp.dest('./build/js'));
});

gulp.task('build:php', function () {
  return gulp.src(['./src/php/*.php'])
    .pipe(gulp.dest('./build/'));
});

gulp.task('build:bin', function () {
  return gulp.src(['./src/bin/*', './src/*.swf'])
    .pipe(gulp.dest('./build/'));
});


gulp.task('build:index', function () {
  return gulp.src('./src/index.html')
    .pipe(useref())
    .pipe(gulp.dest('build'));
});


gulp.task('default', ['serve', 'watch']);

gulp.task('build', ['clean:build', 'build:html', 'build:css', 'build:js', 'build:js-vendors']);

//gulp.task('production-one', ['clean', 'onefile', 'clean-one', 'tinypng']); //use with "inline" parameter in index.html
//gulp.task('production-min', ['clean', 'minifyall', 'imagemin']); //or 'tinypng' for image compressing

