var transformersControllers = angular.module('transformersControllers', []);

//----------------------------------------------------------------------------------------------------------------------
//каталог
transformersControllers.controller('RobotsCatalogCtrl', function ($scope, $rootScope, $http) {
  $http.get('data/catalog.json').success(function (data) {
    $scope.robotsCatalog = data;
  });
});
//----------------------------------------------------------------------------------------------------------------------
//Tabs-закладки
transformersControllers.controller('TabsCtrl', ['$scope', '$rootScope', '$window', '$timeout', '$location', '$browser', '$interval', '$http',
  function ($scope, $rootScope, $window, $timeout, $location, $browser, $interval, $http) {
    $scope.initApp = function () {
      //console.info('initApp');
      window.stage.destroy();

      $http.get('data/swiffyobject.json').success(function (data) {
        var swiffyobject = data[0];
        try {
          window.stage = new swiffy.Stage(document.getElementById('swiffycontainer'), swiffyobject, {});
          window.stage.start();
        } catch (error) {
          //console.log(error);
        }
      });

      context.tabs.main.category = '';
      context.tabs.main.robotAva = '';

      //context.carouselShowMessages();
      context.loadMessageData();


      //angular.element(document.querySelectorAll('.slider-wrapper')).hide(); .s
      angular.element(document.getElementsByClassName("greeting-set-msg")).addClass('hide');
      angular.element(document.getElementsByClassName("election-block-text2")).addClass('hide');
      angular.element(document.getElementsByClassName("greeting-page")).addClass('hide');
      angular.element(document.getElementsByClassName("modal")).addClass('hide');
      angular.element(document.getElementsByClassName("swf")).addClass('hide');

      angular.element(document.getElementsByClassName("repeatBtn")).addClass('hide');

      angular.element(document.getElementsByClassName("category-page")).removeClass('hide');
    };

    //тут будем хранить промежуточные значения для каждой из вкладок
    var context = $rootScope;
    context.tabs = {
      main: null,
      gift: null
    };
    context.tabs.main = {
      friendId: 0,
      category: '',
      robotAva: '',
      msg: '',
      msgId: 0,
      friendPic: '',
      friendFirstName: '',
      friendLastName: ''
    };

    $scope.isShowScreen1 = true;
    $scope.onOff = false;

    context.parseHash = function (hash) {
      var result = '';
      if (hash) {
        result = hash.split('&');
      }
      return result;
    };

    context.setScreen = function (hash) {
      var params = context.parseHash(hash);
      if (params && params.length) {
        var category, robotName;
        switch (params.length) {
          //устанавливаем категорию в контест, отображаем сраницу с поздравлениями
          case 2: {
            //console.log('Need set 1 screen.');
            category = params[1];
            context.tabs.main.category = category;
            angular.element(document.getElementsByClassName("greeting-page")).removeClass('hide');
            angular.element(document.getElementsByClassName("setFrieselection-block-text3nd")).addClass('hide');
            break;
          }
          //устанавливаем имя робота в контекст, уточняем и устанавливаем категорию
          case 3: {
            //console.log('Need set 2 screen.');
            category = params[1];
            robotName = params[2];
            context.tabs.main.category = category;
            context.tabs.main.robotAva = robotName;
            angular.element(document.getElementsByClassName("setFrieselection-block-text3nd")).removeClass('hide');
            angular.element(document.getElementsByClassName("greeting-set-msg")).removeClass('hide');
            angular.element(document.getElementsByClassName("selection-block-texts")).removeClass('hide');
            //context.carouselShowMessages();
            context.loadMessageData();
            break;
          }
        }
      }
    };

    var prewHash = '';
    $interval(function () {
      var curHash = window.location.hash;
      //значит хеш изменился
      if ((!prewHash && curHash) || (prewHash && curHash && prewHash != curHash)) {
        prewHash = curHash;
        //console.log('hash was changed!');
        context.setScreen(curHash);
      }
      else {
        //console.log('hash was not changed!');
      }
    }, 500);


    context.startCanvasFunc = function () {
      $http.get('data/swiffyobject.json').success(function (data) {
        var swiffyobject = data[0];
        try {
          window.stage = new swiffy.Stage(document.getElementById('swiffycontainer'), swiffyobject, {});
          window.stage.start();
          $http.get('data/greeting-messages.json').success(function (data) {
          });

          setTimeout(function () {
            angular.element(document.getElementsByClassName("main-preloader")).addClass('hide');
          }, 1500);

        } catch (error) {
          //console.log(error);
        }
      });
    }

  }]);
//----------------------------------------------------------------------------------------------------------------------
//Carousel - слайдер
transformersControllers.controller('MessagesCtrl', function ($scope, $rootScope, $http, $timeout) {
  var context = $rootScope;

  //context.tabs.main.category, context.tabs.main.robotAva

  context.loadMessageData = function () {
    angular.element(document.getElementsByClassName("slider-wrapper")).addClass('hide');

    $http.get('data/greeting-messages.json').success(function (data) {
      //console.error('loaddata');
      var filtered = $.grep(data, function (element, index) {
        return (element.type === context.tabs.main.category) && ( (element.name === 'common') || (element.name === context.tabs.main.robotAva));
      });

      $scope.messages = filtered;

      $timeout(function () {
        //console.error();
        angular.element(document.querySelectorAll('.sp')).addClass('hide');

        angular.element(document.querySelectorAll('.sp')).hide();

        angular.element(document.querySelectorAll('.sp')[0]).addClass('active-slide');

        angular.element(document.querySelectorAll('.active-slide')).show();
      }, 100);

      $timeout(function () {
        angular.element(document.querySelectorAll('.sp')).removeClass('hide');
        angular.element(document.getElementsByClassName("slider-wrapper")).removeClass('hide');
      }, 500);
    });
  };
});
transformersControllers.controller('CarouselCtrl', ['$scope', '$rootScope', '$http',
  function ($scope, $rootScope, $http) {
    var context = $rootScope;

    $scope.myInterval = 0;
    $scope.noWrapSlides = false;

    function loadMessages(rType, rName) {
      $http.get('data/greeting-messages.json').success(function (data) {
        //console.info('messages loaded');

        var filtered = $.grep(data, function (element, index) {
          return (element.type === rType) && ( (element.name === 'common') || (element.name === rName));
        });

        $scope.slides = filtered;

        $scope.slides[5].active = true;
        //$scope.slides = data;
        try {
          context.tabs.main.msg = data[0].text;
          context.tabs.main.msgId = data[0].id;
        } catch (ex) {
          //console.log(ex);
        }

        console.info('filtered', filtered);
      });
    }

    //console.info('!!!!', context.tabs.main.category, context.tabs.main.robotAva);
    context.carouselShowMessages = function () {
      loadMessages(context.tabs.main.category, context.tabs.main.robotAva);
    };

  }]);
//----------------------------------------------------------------------------------------------------------------------
//Модальное окошка выбора друга
transformersControllers.controller('ModalCtrlFull', function ($scope, $rootScope) {
  var context = $rootScope;

  $scope.open = function () {

    //angular.element(document.getElementsByClassName("category-page")).addClass('hide');
    angular.element(document.getElementsByClassName("myModal")).removeClass('hide');

    //var msg = angular.element(document.getElementsByClassName("item text-center ng-isolate-scope active")).children().children().last().text();
    //active-slide
    var msg = angular.element(document.getElementsByClassName("active-slide")).html();
    context.tabs.main.msg = msg;
    //var msgid = angular.element(document.getElementsByClassName("item text-center ng-isolate-scope active")).children().children().last().attr('msgId');
    var msgid = angular.element(document.getElementsByClassName("active-slide")).attr('msgId');
    context.tabs.main.msgid = msgid;

  };

  $scope.closeModal = function () {
    angular.element(document.getElementsByClassName("myModal")).addClass('hide');
  };

  //$scope.items = items;
  $scope.selected = {};

  $scope.setFriend = function (friendId) {
    //console.log(friendId);
    context.tabs.main.friendId = friendId;
    angular.element(document.getElementsByClassName("startSwf")).removeClass('hide');
  };

  $scope.ok = function () {
    context.tabs.main.friendPic = $scope.selected.friend.photo_200;
    context.tabs.main.friendFirstName = $scope.selected.friend.first_name;
    context.tabs.main.friendLastName = $scope.selected.friend.last_name;


    //$uibModalInstance.close($scope.selected.item);
    //прячем область выбора поздравительных параметров и показываем область с флешкой
    angular.element(document.getElementsByClassName("category-page")).addClass('hide');
    angular.element(document.getElementsByClassName("greeting-page")).addClass('hide');
    angular.element(document.getElementsByClassName("autobot-list")).addClass('hide');
    angular.element(document.getElementsByClassName("decepticon-list")).addClass('hide');
    angular.element(document.getElementsByClassName("swf")).removeClass('hide');
    //отображаем кнопки "еще раз", "на стену"
    angular.element(document.getElementsByClassName("againBtn")).html('СДЕЛАТЬ НОВУЮ');
    angular.element(document.getElementsByClassName("againBtn")).removeClass('again-button2');
    angular.element(document.getElementsByClassName("againBtn")).removeClass('hide');
    angular.element(document.getElementsByClassName("onWallBtn")).removeClass('hide');

    //вызываем функцию флешки
    var params = context.tabs.main;

    //'&characterFile=' + params.robotAva.substring(10, params.robotAva.length) +

    //клеим и подставляем параметры для флешки 8 символов порезать (потом вернуть)
    var flashvars = 'vkId=' + params.friendId +
      '&robotType=' + params.category +
      '&characterFile=' + params.robotAva + '.png' +
      '&cardText=' + params.msgid +
      '&pic=' + params.friendPic +
      '&fName=' + params.friendFirstName +
      '&lName=' + params.friendLastName;

    //angular.element(document.getElementsByName('FlashVars')[0]).attr('value', flashvars);

    $('.flashContent').append('<object class="flash-object" type="application/x-shockwave-flash" data="swf.swf" id="swf" height="661" width="627"><param name="movie" value="swf.swf"><param name="quality" value="high"><param name="bgcolor" value="#ffffff"><param name="play" value="true"><param name="loop" value="true"><param name="wmode" value="opaque"><param name="scale" value="showall"><param name="menu" value="true"><param name="devicefont" value="false"><param name="salign" value=""><param name="allowscriptaccess" value="always"><param name="flashvars" value="' + flashvars + '"></object>');
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel')
  };

});
//----------------------------------------------------------------------------------------------------------------------
//действия после просмотра превью
transformersControllers.controller('PublishCtrl', function ($scope, $rootScope, $http) {

  var context = $rootScope;

  $scope.again = function () {
    // angular.element(document.getElementsByClassName("swf")).addClass('hide');
    // angular.element(document.getElementsByClassName("category-page")).removeClass('hide');
    location.reload();
  };
  $scope.onWall = function () {

    var robotSharePic = '';

    var params = context.tabs.main;
    //var msg = params.msgid ? escape(params.msgid) : '';
    var msg = params.msgid;

    switch (params.robotAva) {
      case 'bumblebee':
        robotSharePic = 'photo-55403010_390029412';
        break;
      case 'fixit':
        robotSharePic = 'photo-55403010_390029416';
        break;
      case 'grimlock':
        robotSharePic = 'photo-55403010_390029417';
        break;
      case 'optimusprime':
        robotSharePic = 'photo-55403010_390029418';
        break;
      case 'sideswipe':
        robotSharePic = 'photo-55403010_390029421';
        break;
      case 'strongarm':
        robotSharePic = 'photo-55403010_390029448';
        break;
      case 'steeljaw':
        robotSharePic = 'photo-55403010_390029437';
        break;
      case 'thunderhoof':
        robotSharePic = 'photo-55403010_390029459';
        break;
      case 'underbite':
        robotSharePic = 'photo-55403010_390029464';
        break;
      default:
        robotSharePic = 'photo-55403010_390029430';
    }

    var tempURL = 'vk.com/app5169535#swf&' +
      params.friendId + '&' +
      params.category + '&' +
      params.robotAva + '.png' + '&' +
      msg + '&' +
      params.friendPic + '&' +
      encodeURI(params.friendFirstName) + '&' +
      encodeURI(params.friendLastName);

    console.log(tempURL);

    var dataString = {
      'vkId': params.friendId,
      'robotType': params.category,
      'characterFile': params.robotAva,
      'cardText': msg,
      'pic': params.friendPic,
      'fName': encodeURI(params.friendFirstName),
      'lName': encodeURI(params.friendLastName)
    };

    $.ajax({
      type: "POST",
      url: "php/writelink.php",
      data: dataString,
      success: function (response) {

        console.info(response);

        VK.api("wall.post", {
          owner_id: params.friendId,
          //attachments: robotSharePic + ',https://' + tempURL,
          attachments: robotSharePic,
          message: 'Моё поздравление от Трансформеров внутри! Скорее жми на ссылку, а то всё пропустишь &#128073;vk.com/app5169535#message' + response
        }, function (data) {
          if (data.error.error_code === 214) {
            console.log(data);
            angular.element(document.getElementsByClassName("wall-warning")).removeClass('hide');
            //alert('стена закрыта');
          }
        });
      }
    });

  };

  $scope.wallWarning = function () {
    angular.element(document.getElementsByClassName("wall-warning")).addClass('hide');
    //console.info('стена закрыта');
  };

  $scope.repeat = function () {
    location.reload();
  };
});

//----------------------------------------------------------------------------------------------------------------------
//список друзей пользователя вк
transformersControllers.controller('VkFriends', function ($scope, fVk) {
  $scope.friends = fVk.getFriends($scope);
});

transformersControllers.factory('fVk', ['$http',
  function ($http) {
    return {
      getFriends: function ($scope) {
        VK.api('users.get', {
          fields: 'id'
        }, function (data) {
          if (data.response) {

            var host = 'https://api.vk.com/method/',
              methodName = 'friends.get',
              uid = 'user_id=' + data.response[0].id,
              fields = 'fields=photo_50,photo_200,first_name,last_name',
              order = '&order=hints&',
              callback = '&callback=JSON_CALLBACK';

            var url = host + methodName + '?' + uid + order + fields + callback;

            $http.jsonp(url).success(function (response) {
              $scope.friends = response.response;
            });
          }
        });
      }
    };
  }
]);

VK.init(function () {
  //console.log('=== API initialization succeeded');
}, function () {
  //console.log('=== VK API initialization failed');
}, '5.40');

//----------------------------------------------------------------------------------------------------------------------

