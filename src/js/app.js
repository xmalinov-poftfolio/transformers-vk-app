angular.element(document).ready(function () {

  var hrefParams = window.location.search;

  if (hrefParams && hrefParams.search(/hash=message/) !== -1) {
    var paramStr = hrefParams.substring(hrefParams.search(/hash=/));
    var messageID = paramStr.substr(12);

    var dataString = {'id': messageID};
    console.info(dataString);

    $.ajax({
      type: "POST",
      url: "php/getlink.php",
      data: dataString,
      success: function (response) {
        console.info(response[0]);

        //клеим и подставляем параметры для флешки
        var flashvars = 'vkId=' + response[0].vkId +
          '&robotType=' + response[0].robotType +
          '&characterFile=' + response[0].characterFile + '.png' +
          '&cardText=' + response[0].cardText +
          '&pic=' + response[0].pic +
          '&fName=' + response[0].fName +
          '&lName=' + response[0].lName;

        angular.element(document.getElementsByClassName("category-page")).addClass('hide');

        //angular.element(document.getElementsByName('FlashVars')[0]).attr('value', flashvars);
        $('.flashContent').append('<object class="flash-object" type="application/x-shockwave-flash" data="swf.swf" id="swf" height="661" width="627"><param name="movie" value="swf.swf"><param name="quality" value="high"><param name="bgcolor" value="#ffffff"><param name="play" value="true"><param name="loop" value="true"><param name="wmode" value="opaque"><param name="scale" value="showall"><param name="menu" value="true"><param name="devicefont" value="false"><param name="salign" value=""><param name="allowscriptaccess" value="always"><param name="flashvars" value="' + flashvars + '"></object>');
        //показываем блок с флешкой
        angular.element(document.getElementsByClassName("swf")).removeClass('hide');

        angular.element(document.getElementsByClassName("againBtn")).html('СДЕЛАЙ СВОЮ ОТКРЫТКУ');
        angular.element(document.getElementsByClassName("againBtn")).addClass('again-button2');
        angular.element(document.getElementsByClassName("againBtn")).removeClass('hide');

        angular.element(document.getElementsByClassName("repeatBtn")).removeClass('hide');

        yaCounter34351530.reachGoal('MESSAGE_VIEW');
      }
    });

  } else {
    angular.element(document.getElementsByClassName("category-page")).removeClass('hide');
  }

  if (hrefParams && hrefParams.search(/hash=swf/) !== -1) {

    //вытаскиваем параметры для флешки
    var paramStr = unescape(hrefParams.substring(hrefParams.search(/hash=swf/)));

    var params = paramStr.split('&');

    var friendId = params[1],
      category = params[2],
      characterFile = params[3],
      //msg = params[4] ? unescape(unescape(params[4])) : '',
      msg = params[4],
      pic = params[5],
      name1 = params[6],
      name2 = params[7];
    //вытаскиваем текст сообщения

    //клеим и подставляем параметры для флешки
    var flashvars = 'vkId=' + friendId +
      '&robotType=' + category +
      '&characterFile=' + characterFile +
      '&cardText=' + msg +
      '&pic=' + pic +
      '&fName=' + decodeURI(name1) +
      '&lName=' + decodeURI(name2);

    angular.element(document.getElementsByClassName("category-page")).addClass('hide');

    angular.element(document.getElementsByName('FlashVars')[0]).attr('value', flashvars);
    //показываем блок с флешкой
    angular.element(document.getElementsByClassName("swf")).removeClass('hide');

    angular.element(document.getElementsByClassName("againBtn")).html('СДЕЛАЙ СВОЮ ОТКРЫТКУ');
    angular.element(document.getElementsByClassName("againBtn")).addClass('again-button2');
    angular.element(document.getElementsByClassName("againBtn")).removeClass('hide');

    angular.element(document.getElementsByClassName("repeatBtn")).removeClass('hide');

  } else {
    angular.element(document.getElementsByClassName("category-page")).removeClass('hide');
  }

  var context = angular.element(document.getElementsByClassName("TabsCtrlClass")).scope();
  context.startCanvasFunc();

  //console.info('dom loaded ===');

});

var transformersApp = angular.module('transformersApp', [
  'ngRoute',
  'transformersControllers',
  'ui.bootstrap',
  'ngAnimate'
]);

$(document).ready(function () {
  $('.sp').first().addClass('active-slide');
  $('.sp').hide();
  $('.active-slide').show();

  $('#button-next').click(function () {
    //console.info('next');
    $('.active-slide').removeClass('active-slide').addClass('oldActive-slide');
    if ($('.oldActive-slide').is(':last-child')) {
      $('.sp').first().addClass('active-slide');
    } else {
      $('.oldActive-slide').next().addClass('active-slide');
    }
    $('.oldActive-slide').removeClass('oldActive-slide');
    //$('.sp').fadeOut(100);
    //$('.active-slide').fadeIn(100);
    $('.sp').hide();
    $('.active-slide').show();
  });

  $('#button-previous').click(function () {
    //console.info('prev');
    $('.active-slide').removeClass('active-slide').addClass('oldActive-slide');
    if ($('.oldActive-slide').is(':first-child')) {
      $('.sp').last().addClass('active-slide');
    } else {
      $('.oldActive-slide').prev().addClass('active-slide');
    }
    $('.oldActive-slide').removeClass('oldActive-slide');
    //$('.sp').fadeOut(100);
    //$('.active-slide').fadeIn(100);
    $('.sp').hide();
    $('.active-slide').show();
  });
});

